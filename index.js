'use strict'

const soap = require('soap')
const { v4: uuid } = require('uuid')
const convert = require('xml-js')

// JASPER SOAP WSDLs
const BILLING_WSDL = process.env.BILLING_WSDL ? process.env.BILLING_WSDL : __dirname + '/wsdl/billing.wsdl'

class JasperSoapError extends Error {
  constructor(message, code) {
    super(message)
    this.code = code
  }
}

class JasperSoap {

  constructor(wsdl) {
    this.wsdl = wsdl
  }

  /**
   * Initialise SOAP API client
   * @param {*} licenseKey 
   * @param {*} username 
   * @param {*} password 
   * @returns 
   */
  init(licenseKey, username, password) {
    var self = this
    this.licenseKey = licenseKey
    return new Promise((resolve, reject) => {
      soap.createClientAsync(self.wsdl, {overridePromiseSuffix: 'Promise'}).then((client) => {
        self.client = client
        self.client.setSecurity(new soap.WSSecurity(username, password, {}))
        resolve()
      }).catch((err) => {
        reject(err)
      })
    })
  }

  /**
   * Call a SOAP API Function
   * @param {*} func - SOAP Function to call
   * @param {*} funcArgs  - function arguments
   * @returns 
   */
  callAPI(func, funcArgs) {
    var self = this
    return new Promise((resolve, reject) => {
      // create args from common args plus function specific ones
      var args = {
        messageId: uuid(),
        version: '',
        licenseKey: self.licenseKey,
        ...funcArgs,
      }
      // call SOAP API function with args
      func(args, function(err, result) {
        if (err) 
          reject(self.parseError(err))
        else
          resolve(result)
      })
    })
  }

  /**
   * Handle SOAP API error
   * @param {*} err 
   * @returns 
   */
  parseError(err) {
    if(err.body) {
      try {
        const body = JSON.parse(convert.xml2json(err.body, {compact: true}))
        const fault = body["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["SOAP-ENV:Fault"]
        return new JasperSoapError(fault.detail['jws:error']._text, fault.faultstring._text)
      } catch (error) {
        return err
      }
    }
    return err
  }

}

class JasperBilling extends JasperSoap {

  constructor() {
    super(BILLING_WSDL)
  }

  async getUsage(iccid, date) {
    const result = await this.callAPI(this.client.GetTerminalUsage, {
      iccid: iccid,
      cycleStartDate: date,
    })
    return { data: result }
  }

  async getDataUsageDetails(iccid, date, page) {
    const result = await this.callAPI(this.client.GetTerminalUsageDataDetails, {
      iccid: iccid,
      cycleStartDate: date,
      pageNumber: page,
    })
    return { data: result.usageDetails.usageDetail, pages: result.totalPages }
  }

  async getSmsUsageDetails(iccid, date, page) {
    const result = await this.callAPI(this.client.GetTerminalUsageSmsDetails, {
      iccid: iccid,
      cycleStartDate: date,
      pageNumber: page,
    })
    return { data: result.smsUsageDetails.smsUsageDetail, pages: result.totalPages }
  }

}

module.exports = {JasperBilling, JasperSoapError}